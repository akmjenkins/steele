# README.md

## Grunt, GIT, and Front End Assets

### Instructions - **The *EASY* Way**

* Open up terminal
* Create a new directory and run
`git clone https://bitbucket.org/akmjenkins/projectname ./projectname` (checks out the tree into a folder `./projectname` - the precise name of the folder doesn't matter.)
* Navigate to `./projectname/`
* Run `sh mac_setup.sh`

This will install the following software (if necessary):

* `homebrew` - package manager for OS X, makes setting up the rest of the template simple
* `nodejs`
* `bower` - package manager (used by codekit) (installs globally)
* `grunt-cli` - grunt command line tool (installs globally)
* `ImageMagick` - used to compile favicons from sources
* `GraphicsMagick` - used to compile optimized sprite sheets

The end of this script will run `grunt dev-watch` in the `./assets` which watches any assets added/modified in the src folder and recompiles SASS, JS, updates sprite sheets, etc whenever anything is changed and automatically places it in the `./assets/bin` directory

###Grunt Tasks
---

## For development
* Inside the `assets` directory run `grunt dev-watch`
* Any time you edit/add a CSS file in `src/css` - `bin/style.css` will be recompiled automatically.
* JS is concatenated and minified using [requirejs](http://requirejs.org/) - take a look at `src/js/main.js` and the files it requires to get a handle on the `define` and `require` functions.
* Any time you update `src/js/main.js` - `bin/js/main.js` will be recompiled automatically

## For distribution
* Inside the `assets` directory run `grunt prod`
* Only ever upload `assets/bin` to the live server
* Note: You'll need to put your [TinyPNG API Key](https://tinypng.com/developers) in `assets/user.defaults.json` in order to get TinyPNG to losslessly compress any PNG image assets (e.g. sprites) in the `assets/bin` folder

####Compilation Tasks
---
##### dev `(targets: none)` - this is the default task
##### dev-watch `(targets: none)`
##### prod-watch `(targets: none)`
##### compress-dev `(targets: none)`
##### compress-prod `(targets: none)`


####Individual Tasks
---
##### tinypng `(targets: compress)`
##### requirejs `(targets: dev, dist)`
##### bower `(targets: install)`
##### spriteGenerator `(targets: regular,retina)`
##### compress `(targets: main)`
##### uglify `(targets: dev, dist)`
##### modernizr `(targets: dist)`
##### watch `(targets: css,js,fonts,sprites,images)`
##### copy `(targets: images,fonts,js,process_sprite_css)`
##### clean `(targets: all,images,css,js,bower,fonts)`

### Notes
* **NEVER** edit/add anything inside the `assets/bin` directory - you will lose it.
* **ALWAYS** commit your work to the repo. 