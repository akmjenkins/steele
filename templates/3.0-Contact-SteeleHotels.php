<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/bin/images/temp/hero/hero-inside-4.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Contact</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<article>
	
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite ib home replace">Home</a>
					<a href="#">Contact</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<section class="light contact-locations-wrap">
				<div class="sw">
					
					<!-- fake location selector -->
					<div class="contact-locations">
						<div class="grid nopad">
							<div class="col-6 col">
								<div class="loc selected">
									<div class="hotel-logo">
										<img src="../assets/bin/images/hotels/jag-color.svg" alt="JAG">

										<span class="rating">
											&#9733;
											&#9733;
											&#9733;
											&#9733;
											&#9733;
										</span>
									</div><!-- .hotel-logo -->

									<address>
										1 This Street <br>
										St. John's, NL <br>
										709-256-2406
									</address>

									<a href="#" class="button dark-fill sm">Book Now</a>
								</div>
							</div><!-- .col -->

							<div class="col-6 col">
								<div class="loc">
									<div class="hotel-logo">										
										<img src="../assets/bin/images/hotels/the-capital-color.svg" alt="The Capital">

										<span class="rating">
											&#9733;
											&#9733;
											&#9733;
											&frac12;
										</span>
									</div><!-- .hotel-logo -->

									<address>
										208 Kenmount Road <br>
										St. John's, NL <br>
										1-800-503-1603
									</address>

									<a href="#" class="button dark-fill sm">Book Now</a>
								</div>
							</div><!-- .col -->

							<div class="col-6 col">
								<div class="loc">
									<div class="hotel-logo">										
										<img src="../assets/bin/images/hotels/the-albatross-dark.svg" alt="The Albatross">

										<span class="rating">
											&#9733;
											&#9733;
											&#9733;
											&frac12;
										</span>
									</div><!-- .hotel-logo -->											

									<address>
										114 Trans Canada Hwy <br>
										Gander, NL <br>
										1-800-503-1603
									</address>

									<a href="#" class="button dark-fill sm">Book Now</a>
								</div>
							</div><!-- .col -->			

							<div class="col-6 col">
								<div class="loc">
									<div class="hotel-logo">										
										<img src="../assets/bin/images/hotels/sinbads-dark.svg" alt="Sinbads Hotel and Suites">

										<span class="rating">
											&#9733;
											&#9733;
											&#9733;
											&frac12;
										</span>
									</div><!-- .hotel-logo -->

									<address>
										133 Bennett Drive <br>
										Gander, NL <br>
										1-800-503-1603
									</address>

									<a href="#" class="button dark-fill sm">Book Now</a>
								</div>
							</div><!-- .col -->	

							<div class="col-6 col">
								<div class="loc">
									<div class="hotel-logo">										
										<img src="../assets/bin/images/hotels/irving-west-dark.svg" alt="The Irving West">

										<span class="rating">
											&#9733;
											&#9733;
											&frac12;
										</span>
									</div><!-- .hotel-logo -->

									<address>
										1 Caldwell Street <br>
										Gander, NL <br>
										709-256-2406
									</address>

									<a href="#" class="button dark-fill sm">Book Now</a>
								</div>
							</div><!-- .col -->		

							<div class="col-6 col">
								<div class="loc">
									<div class="hotel-logo">
										<img src="../assets/bin/images/hotels/glynmill-inn-dark.svg" alt="The Glynmill Inn">

										<span class="rating">
											&#9733;
											&#9733;
											&#9733;
											&frac12;
										</span>
									</div><!-- .hotel-logo -->

									<address>
										1 Cobb Lane <br>
										Corner Brook, NL <br>
										709-634-5181
									</address>

									<a href="#" class="button dark-fill sm">Book Now</a>
								</div>
							</div><!-- .col -->										

						</div><!-- .grid -->
					</div><!-- .contact-locations -->
				</div><!-- .sw -->
			</section><!-- .light -->
			
			<section class="sw cf">
				<div class="main-body">
					<div class="article-body">
						
						<p>
							Sed nec tincidunt ipsum. Nullam a ligula eget velit gravida adipiscing et ut turpis. In hac habitasse platea dictumst. 
							Nam tincidunt tellus sit amet pellentesque semper. Morbi at porttitor magna. Aliquam tincidunt velit ac sem porta, 
							a sagittis ante facilisis. In faucibus purus a enim accumsan laoreet sed vitae tortor. Aliquam in risus placerat, 
							malesuada ante id, egestas elivt.
						</p>
						
						<form action="/" method="post" class="body-form full contact-form">
							<fieldset class="grid pad5 collapse-850">
								<div class="col-2 col">
									<input type="text" name="name" placeholder="Name">
									<input type="email" name="email" placeholder="Email">
									<input type="tel" pattern="\d+" name="phone" placeholder="Phone">
									
									<!-- real location selector -->
									<select name="location" class="i-hidden">
										<option value="">Choose Location</option>
										<option value="jag" selected>JAG</option>
										<option value="capital">The Capital Hotel</option>
										<option value="albatross">The Albatross</option>
										<option value="sinbads">Sinbad's</option>
										<option value="irving-west">The Irving West</option>
										<option value="glynmill-inn">The Glynmill Inn</option>
										<option value="hotel-gander">Hotel Gander</option>
									</select>
									
								</div><!-- .col -->
								<div class="col-2 col">
									<textarea name="message" cols="30" rows="10" placeholder="Message"></textarea>
								</div><!-- .col -->
							</fieldset><!-- .grid -->
							
							<button type="submit" class="button">Send Message</button>
						</form><!-- .body-form -->
						
					</div><!-- .article-body -->
				</div><!-- .main-body -->
			</section><!-- .sw -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>