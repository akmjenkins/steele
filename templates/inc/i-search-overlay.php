			<div class="search-overlay no-results">
				
				
				<div class="search-content">


					<!-- 
						ALL YOU HAVE TO DO TO GET THE SEARCH FORM WORKING IS RETURN THE HTML IN i-search-results.php
						FROM THE URL THAT THE action ATTRIBUTE OF THIS FORM POINTS TO WITH A QUERY STRING OF ?q=query

						i.e. FOR THE BELOW FORM, THE SEARCH JS WILL GO AND GRAB inc/i-search-results.php?q=Your Query
					-->
					
					<form action="./inc/i-search-results.php" class="search-form">
						<input type="text" name="search" placeholder="Search Steele Hotels">
						<button type="button" class="toggle-search-form close sprite-before abs">Close</button>

						<div class="loader sprite-before abs spinner">
							<img src="../assets/bin/images/spinner.gif" alt="loading">
						</div><!-- .spinner -->

					</form><!-- .search-form -->
					
					<div class="search-results">
						&nbsp;
					</div><!-- .search-results -->
					
				</div><!-- .search-content -->
				
			</div><!-- .search-overlay -->