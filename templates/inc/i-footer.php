			<footer>
				<div class="sw">
					
					<a href="#" class="logo">Steele Hotels</a>

					<div class="footer-contact">

						<address>
							Steele Hotels <br>
							123 Address Street <br>
							St. John's, NL  A1N 1N1
						</address>

						<div class="phones">
							<span class="block">
								<span>Phone</span>
								1 800 000 0000
							</span>
							<span class="block">
								<span>Fax</span>
								1 800 000 0000
							</span>							
						</div><!-- .phones -->

						<a href="#">info@steelehotels.com</a>

					</div><!-- .footer-contact -->

					<div>
						
						<div class="footer-nav">
							<ul>
								<li><a href="#">Our Location</a></li>
								<li><a href="#">Meetings &amp; Events</a></li>
								<li><a href="#">Promotions</a></li>
								<li><a href="#">Reservations</a></li>
								<li><a href="#">The Latest</a></li>
							</ul>

							<?php include('i-social.php'); ?>
						</div><!-- .footer-nav -->

						<div class="footer-hotels">
							<ul>
								<li><a href="#"><img src="../assets/bin/images/hotels/jag-light.svg" alt="JAG Hotel St. John's"></a></li>
								<li><a href="#"><img src="../assets/bin/images/hotels/the-capital-light.svg" alt="The Capital Hotel St. John's"></a></li>
								<li><a href="#"><img src="../assets/bin/images/hotels/the-albatross-light.svg" alt="The Albatross Hotel Gander"></a></li>
								<li><a href="#"><img src="../assets/bin/images/hotels/sinbads-light.svg" alt="Sinbad's Hotel & Suites"></a></li>
								<li><a href="#"><img src="../assets/bin/images/hotels/irving-west-light.svg" alt="The Irving West Hotel Gander"></a></li>
								<li><a href="#"><img src="../assets/bin/images/hotels/glynmill-inn-light.svg" alt="The Glynmill Inn"></a></li>
							</ul>

							<div class="affiliated">
								<span>Affiliated</span>
								<ul>
									<li><a href="#"><img src="../assets/bin/images/hotels/hotel-gander-light.svg" alt="Hotel Gander"></a></li>
								</ul>
							</div><!-- .affiliated -->
						</div><!-- .footer-hotels -->


						<div class="copyright">
							<ul>
								<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Steele Hotels</a></li>
								<li><a href="#">Sitemap</a></li>
								<li><a href="#">Feedback</a></li>						
								<li><a href="#">Legal</a></li>
							</ul>
							
							<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac" class="replace">JAC. We Create.</a>
						</div><!-- .copyright -->						

					</div>

				</div><!-- .sw -->
			
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/steele/',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script data-main="../assets/bin/js/main.js" src="../assets/bin/lib/js/requirejs/require.min.js"></script>

		<?php include('i-search-overlay.php'); ?>
	</body>
</html>
