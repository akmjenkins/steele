<div class="nav-wrap">
	<div class="nav">
		<div class="sw">

			<nav>
				<ul>
					<li>
						<a href="#">Our Locations</a>

						<div class="dd-locations">
							
							<div class="grid">
								<div class="col-3 col">
									<div>
										<div class="hotel-logo">
											<a href="#"><img src="../assets/bin/images/hotels/jag-color.svg" alt="JAG"></a>

											<span class="rating">
												&#9733;
												&#9733;
												&#9733;
												&#9733;
												&#9733;
											</span>
										</div><!-- .hotel-logo -->

										<address>
											1 This Street <br>
											St. John's, NL <br>
											709-256-2406
										</address>
										
										<div class="btn-group">
											<a href="#" class="button dark-fill sm">Book Now</a>
											<a href="#" class="button dark-fill sm">More Info</a>
										</div><!-- .btn-group -->
									</div>
								</div><!-- .col -->

								<div class="col-3 col">
									<div>
										<div class="hotel-logo">										
											<a href="#"><img src="../assets/bin/images/hotels/the-capital-color.svg" alt="The Capital"></a>

											<span class="rating">
												&#9733;
												&#9733;
												&#9733;
												&frac12;
											</span>
										</div><!-- .hotel-logo -->

										<address>
											208 Kenmount Road <br>
											St. John's, NL <br>
											1-800-503-1603
										</address>

										<div class="btn-group">
											<a href="#" class="button dark-fill sm">Book Now</a>
											<a href="#" class="button dark-fill sm">More Info</a>
										</div><!-- .btn-group -->
									</div>
								</div><!-- .col -->

								<div class="col-3 col">
									<div>
										<div class="hotel-logo">										
											<a href="#"><img src="../assets/bin/images/hotels/the-albatross-dark.svg" alt="The Albatross"></a>

											<span class="rating">
												&#9733;
												&#9733;
												&#9733;
												&frac12;
											</span>
										</div><!-- .hotel-logo -->											

										<address>
											114 Trans Canada Hwy <br>
											Gander, NL <br>
											1-800-503-1603
										</address>

										<div class="btn-group">
											<a href="#" class="button dark-fill sm">Book Now</a>
										</div><!-- .btn-group -->
									</div>
								</div><!-- .col -->			

								<div class="col-3 col">
									<div>
										<div class="hotel-logo">										
											<a href="#"><img src="../assets/bin/images/hotels/sinbads-dark.svg" alt="Sinbads Hotel and Suites"></a>

											<span class="rating">
												&#9733;
												&#9733;
												&#9733;
												&frac12;
											</span>
										</div><!-- .hotel-logo -->

										<address>
											133 Bennett Drive <br>
											Gander, NL <br>
											1-800-503-1603
										</address>

										<div class="btn-group">
											<a href="#" class="button dark-fill sm">Book Now</a>
											<a href="#" class="button dark-fill sm">More Info</a>
										</div><!-- .btn-group -->
									</div>
								</div><!-- .col -->	

								<div class="col-3 col">
									<div>
										<div class="hotel-logo">										
											<a href="#"><img src="../assets/bin/images/hotels/irving-west-dark.svg" alt="The Irving West"></a>

											<span class="rating">
												&#9733;
												&#9733;
												&frac12;
											</span>
										</div><!-- .hotel-logo -->

										<address>
											1 Caldwell Street <br>
											Gander, NL <br>
											709-256-2406
										</address>

										<div class="btn-group">
											<a href="#" class="button dark-fill sm">Book Now</a>
										</div><!-- .btn-group -->
									</div>
								</div><!-- .col -->		

								<div class="col-3 col">
									<div>
										<div class="hotel-logo">
											<a href="#"><img src="../assets/bin/images/hotels/glynmill-inn-dark.svg" alt="The Glynmill Inn"></a>

											<span class="rating">
												&#9733;
												&#9733;
												&#9733;
												&frac12;
											</span>
										</div><!-- .hotel-logo -->

										<address>
											1 Cobb Lane <br>
											Corner Brook, NL <br>
											709-634-5181
										</address>

										<div class="btn-group">
											<a href="#" class="button dark-fill sm">Book Now</a>
											<a href="#" class="button dark-fill sm">More Info</a>
										</div><!-- .btn-group -->
									</div>
								</div><!-- .col -->										

							</div><!-- .grid -->

							<div class="affiliated-hotels">
								<span class="h3-style">Affiliated Hotels</span>

								<div class="grid">
									<div class="col-3 col">
										<div>
											<div class="hotel-logo">
												<a href="#"><img src="../assets/bin/images/hotels/hotel-gander-color.svg" alt="Hotel Gander"></a>

												<span class="rating">
													&#9733;
													&#9733;
													&#9733;
													&frac12;
												</span>
											</div><!-- .hotel-logo -->

											<address>
												100 Trans Canada Highway <br>
												Gander, NL <br>
												1-800-563-2988
											</address>
											
											<div class="btn-group">
												<a href="#" class="sm button dark-fill" rel="external">Website</a>
											</div><!-- .btn-group -->											

										</div>
									</div><!-- .col -->	
								</div><!-- .grid -->

							</div><!-- .affiliated-hotels -->

						</div><!-- .locations-dd -->

					</li>
					<li>
						<a href="#">Meetings &amp; Events</a>

						<div>

							<ul>
								<li><a href="#">Meetings</a></li>
								<li><a href="#">Special Events</a></li>
								<li><a href="#">Weddings &amp; Banquets</a></li>
							</ul>

							<div class="swiper-wrapper">
								<div class="swipe">
									<div class="swipe-wrap">
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->

						</div>

					</li>
					<li>
						<a href="#">Promotions</a>

						<div>

							<ul>
								<li><a href="#">Promoion #1</a></li>
								<li><a href="#">Promotion #2</a></li>
								<li><a href="#">Promotion #3</a></li>
							</ul>

							<div class="swiper-wrapper">
								<div class="swipe">
									<div class="swipe-wrap">
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->

						</div>

					</li>
					<li>
						<a href="#">Reservations</a>

						<div>

							<ul>
								<li><a href="#">Submenu Item 1</a></li>
								<li><a href="#">Submenu Item 2</a></li>
								<li><a href="#">Submenu Item 3</a></li>
							</ul>

							<div class="swiper-wrapper">
								<div class="swipe">
									<div class="swipe-wrap">
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->

						</div>

					</li>
					<li>
						<a href="#">The Latest</a>

						<div>

							<ul>
								<li><a href="#">Submenu Item 1</a></li>
								<li><a href="#">Submenu Item 2</a></li>
								<li><a href="#">Submenu Item 3</a></li>
							</ul>

							<div class="swiper-wrapper">
								<div class="swipe">
									<div class="swipe-wrap">
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
										<div>
											
											<div class="img" data-img="assets/bin/images/temp/dropdown.jpg">&nbsp;</div>
											<div class="meta">
												<span class="dd-title">Integer Fringilla sit amet justo scelerisque consectetur</span>
												<p>ACras at arcu ut ipsum dictum porttitor sit amet quis magna. Cras non enim ut nisi volutpat varius. Sed ac porta neque. Etiam vulputate quis est eget venenatis. Suspendisse vitae hendrerit lectus.</p>
											</div><!-- .meta -->

										</div>
									</div><!-- .swipe-wrap -->
								</div><!-- .swipe -->
							</div><!-- .swiper-wrapper -->

						</div>

					</li>			
				</ul>
			</nav>

			<button id="toggle-search" class="toggle-search-form sprite-before abs search">Search</button>

			<div class="top">
			
				<a href="#">Media</a>
				<a href="#">Contact</a>
				<a href="#">Careers</a>

				<?php include('i-social.php'); ?>

			</div><!-- .top -->
			
		</div><!-- .sw -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->