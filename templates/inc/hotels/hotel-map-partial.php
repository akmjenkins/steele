<?php include dirname(__FILE__) . "/../api-keys.php"; ?>

<div class="tab-wrapper">
	<div class="tab-controls direction-controls">
	
		<div class="selector with-arrow responsive-tab-selector">
			<select class="tab-controller">
				<option selected>Shopping</option>
				<option>Dining</option>
				<option>Activities</option>
			</select>
			<span class="value">&nbsp;</span>
		</div><!-- .selector -->
	
		<div class="tbl-wrap">
			<div class="tbl">
			
				<div class="button dark-fill tab-control col-3 center direction-button selected">
					<span class="sprite-before shopping">Shopping</span>
				</div>
				
				<div class="button dark-fill tab-control col-3 center direction-button">
					<span class="sprite-before cutlery">Dining</span>
				</div>
				
				<div class="button dark-fill tab-control col-3 center direction-button">
					<span class="sprite-before activities">Activities</span>
				</div>
				
			</div>
		</div>
	
	</div><!-- .tab-controls -->
	<div class="tab-holder">
		<div class="tab selected">
			<div class="embedded-gmap">
				<iframe
					frameborder="0" style="border:0"
					src="https://www.google.com/maps/embed/v1/search?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>&zoom=13&center=48.9494483,-57.9418742&q=shopping">
				</iframe>
			</div><!-- .embedded-gmap -->
		</div><!-- .tab -->
		<div class="tab">
		
			<div class="embedded-gmap">
				<iframe
					frameborder="0" style="border:0"
					src="https://www.google.com/maps/embed/v1/search?key=<?php echo $GOOGLE_MAPS_API_KEY; ?>&zoom=13&center=48.9494483,-57.9418742&q=restaurants">
				</iframe>
			</div><!-- .embedded-gmap -->
			
		</div><!-- .tab -->
		<div class="tab">
		
			<div class="grid">
				<div class="col-2 col sm-col-1">
				
					<h3>What are you looking for?</h3>
					
					<p>Type your keywords or phrase into our search box to quickly find what you are looking for.</p>
				</div><!-- .col -->
				<div class="col-2 col sm-col-1">
					<form action="/" class="single-form" id="activities-form">
						<fieldset>
							<input type="text" placeholder="Enter search terms...">
							<button type="submit|" class="sprite-before abs search"></button>
						</fieldset>
					</form>
				</div><!-- .col -->
			</div><!-- .grid -->
		
			<div id="activities-map" class="embedded-gmap" data-zoom="13" data-center="48.9494483,-57.9418742">
				
			</div><!-- #activites-map -->
		</div>
	</div><!-- .tab-holder -->
</div><!-- .tab-wrapper -->