<?php include dirname(__FILE__) . "/../api-keys.php"; ?>

<?php include('hotel-gallery.php'); ?>

<hr />

<img src="../assets/bin/images/temp/hotel-gallery/sidebar-2.jpg" class="alignright">

<div class="hgroup">
	<h2>About Accommodations</h2>
	<h5 class="light">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5>
</div><!-- .hgroup -->

<p>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra 
	justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. 
	Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
</p>

<p>
	Phasellus at commodo libero, nec sollicitudin nibh. Aenean at lectus scelerisque, porta urna tincidunt, dictum lectus. Mauris faucibus ut nibh quis mollis. 
	Maecenas mattis, urna eleifend ullamcorper fermentum, nisl velit fermentum nunc, sed adipiscing dolor erat ut lorem. Nulla sagittis ipsum a ultricies accumsan.
</p>

<hr />

<div class="hgroup">
	<h2>Accommodations Features</h2>
	<h5 class="light">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h5>
</div><!-- .hgroup -->

<div class="feature-block">
	<div class="img-wrap">
		<div class="pad-thumb square" style="background-image: url(../assets/bin/images/temp/hotel-gallery/feat-1.jpg);"></div>
	</div>
	
	<span class="h4-style">Feature Name</span>
	<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
	quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
</div><!-- .feature-block -->

<div class="feature-block">
	<div class="img-wrap">
		<div class="pad-thumb square" style="background-image: url(../assets/bin/images/temp/hotel-gallery/feat-2.jpg);"></div>
	</div>
	
	<span class="h4-style">Feature Name</span>
	<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
	quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
</div><!-- .feature-block -->

<div class="feature-block">
	<div class="img-wrap">
		<div class="pad-thumb square" style="background-image: url(../assets/bin/images/temp/hotel-gallery/feat-3.jpg);"></div>
	</div>
	
	<span class="h4-style">Feature Name</span>
	<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
	quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
</div><!-- .feature-block -->

<div class="feature-block">
	<div class="img-wrap">
		<div class="pad-thumb square" style="background-image: url(../assets/bin/images/temp/hotel-gallery/feat-4.jpg);"></div>
	</div>
	
	<span class="h4-style">Feature Name</span>
	<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
	quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
</div><!-- .feature-block -->

<div class="feature-block">
	<div class="img-wrap">
		<div class="pad-thumb square" style="background-image: url(../assets/bin/images/temp/hotel-gallery/feat-5.jpg);"></div>
	</div>
	
	<span class="h4-style">Feature Name</span>
	<p>Maecenas convallis interdum ullamcorper. Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. Curabitur id justo 
	quis eros ornare porta a quis tellus. Donec tristique aliquam tortor id ultrices.</p>
</div><!-- .feature-block -->