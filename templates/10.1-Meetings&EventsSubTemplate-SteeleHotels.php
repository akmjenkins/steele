<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/bin/images/temp/hero/hero-inside-16.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Meetings</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<article>
	
			<div class="breadcrumbs">
				<div class="sw">
					<a href="#" class="sprite ib home replace">Home</a>
					<a href="#">Meetings &amp; Events</a>
					<a href="#">Meetings</a>
				</div><!-- .sw -->
			</div><!-- .breadcrumbs -->
			
			<section class="sw cf">
				<div class="main-body with-sidebar">
					<div class="article-body">
						
						<p class="excerpt">
							Donec at augue nec ante hendrerit venenatis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec adipiscing ut sem tempus sodales. 
						</p>
						
						<p>
							Nullam malesuada leo in risus dictum ullamcorper. Fusce elementum, lorem vel varius aliquam, justo massa dignissim tortor, in tempor eros arcu nec ipsum. In tempus mattis libero, 
							sit amet placerat nisl ultrices in. Nulla a fermentum sem. Proin in diam ut enim tristique lobortis. Phasellus porta mollis erat, quis porttitor purus vehicula eu. Vestibulum sit amet 
							lectus magna. Nam et rhoncus turpis. Sed nec feugiat ligula. Donec at erat eros. Quisque eu convallis dui.
						</p>
						
						<h5>Donec at Augue nec Ante</h5>
						
						<p>
							Duis sagittis luctus risus sed ornare. Maecenas sollicitudin purus non tempor facilisis. Mauris a dictum lectus, ac laoreet est. Nulla facilisi. Duis imperdiet, mi at hendrerit hendrerit, 
							tortor lectus pretium odio, mollis pulvinar nisi nisi accumsan nisi. Donec ut aliquet turpis. Cras nec nisi consequat, lacinia purus eget, ullamcorper orci. Vivamus mauris turpis, 
							iaculis nec imperdiet ac, varius ut nunc.
						</p>
							
						<p>
							Quisque feugiat mauris mi, ac fringilla erat rutrum non. Morbi consequat massa in massa euismod, ac suscipit sem aliquam. Sed libero felis, feugiat eu hendrerit sit amet, 
							tincidunt gravida purus. Aenean aliquam erat a tincidunt vestibulum. Curabitur placerat lacus at risus ornare convallis. Nulla auctor quis lorem ac adipiscing. Maecenas 
							convallis et velit ac posuere. Duis dictum felis sit amet dui sollicitudin, eget posuere sapien viverra. Cras porttitor ornare odio, eu faucibus lorem convallis eu.
						</p>
						
						<h5>Photo Gallery</h5>
						
						<div class="gallery-thumbs">
							<div class="gallery-thumb">
								<a href="#" style="background-image: url(../assets/bin/images/temp/gallery-thumb-1.jpg);"></a>
							</div><!-- .gallery-thumb -->
							<div class="gallery-thumb">
								<a href="#" style="background-image: url(../assets/bin/images/temp/gallery-thumb-2.jpg);"></a>
							</div><!-- .gallery-thumb -->
							<div class="gallery-thumb">
								<a href="#" style="background-image: url(../assets/bin/images/temp/gallery-thumb-3.jpg);"></a>
							</div><!-- .gallery-thumb -->
							<div class="gallery-thumb">
								<a href="#" style="background-image: url(../assets/bin/images/temp/gallery-thumb-4.jpg);"></a>
							</div><!-- .gallery-thumb -->
						</div><!-- .gallery -->
						
					</div><!-- .article-body -->
				</div><!-- .main-body -->
				<aside class="sidebar">
					
					<div>
						<a href="#" class="full button dark-fill selected">Meetings</a>
						<a href="#" class="full button dark-fill">Special Events</a>
						<a href="#" class="full button dark-fill">Weddings &amp; Banquest</a>
					</div>
					
					<div class="dark-bg callout-wrap">
						<div class="callout">
							<div class="content">
								<h3>Interest in Hosting Your Meeting With Us?</h3>
								
								<form action="/" method="post" class="body-form cf">
									<fieldset>
										<input type="email" placeholder="Email address...">
										<button type="submit" class="button right dark-bg">Request Info</button>
									</fieldset>
								</form>
								
								<span>Or call us at 1.888.888.8888</span>
							</div><!-- .content -->
						</div><!-- .callout -->
					</div><!-- .dark-bg -->
					
				</aside><!-- .sidebar -->
			</section><!-- .sw -->
		
		</article>
	
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>