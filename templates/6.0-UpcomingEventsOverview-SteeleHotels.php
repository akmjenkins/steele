<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/bin/images/temp/hero/hero-inside-7.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Events</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="sprite ib home replace">Home</a>
				<a href="#">The Latest</a>
				<a href="#">Events</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
	
		<div class="filter-section">
			<div class="filter-bar dark-bg">
				<div class="sw">
					<div class="meta">
					
						<div class="selector with-arrow">
							<select name="sort-by">
								<option>Sort By Hotel</option>
								<option value="jag">JAG</option>
								<option value="capital">The Capital Hotel</option>
								<option value="albatross">The Albatross</option>
								<option value="sinbads">Sinbad's</option>
								<option value="irving-west">The Irving West</option>
								<option value="glynmill-inn">The Glynmill Inn</option>
								<option value="hotel-gander">Hotel Gander</option>								
							</select>
							<span class="value">&nbsp;</span>
						</div><!-- .selector -->
					
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search events...">
								<button type="submit" class="sprite-after abs search">Search</button>
							</fieldset>
						</form>
						
						<div class="controls">
							<button class="sprite arrow-prev-white">Previous</button>
							<button class="sprite arrow-next-white">Next</button>
						</div><!-- .controls -->
						
					</div><!-- .meta -->
				
					<span class="h6-style title">15 Events</span>
				
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-contents">
				<div class="sw">
				
					<div class="article-body">
						<div class="grid eqh collapse-no-flex blocks collapse-800">
						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/bin/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<span class="h3-style title">Morbi malesuada nibh non blandit semper</span>
											<span class="h5-style light subtitle">Etiam enim lorem, aliquam a iaculis</span>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col-3 -->
							
							<div class="col-3 col">							
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/bin/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<span class="h3-style title">Morbi malesuada nibh non blandit semper</span>
											<span class="h5-style light subtitle">Etiam enim lorem, aliquam a iaculis</span>
											<p>Donec nec sodales ante. Integer nisi mi, mattis ac accumsan sed, semper sed velit. Pellentesque at porttitor velit. Donec ac facilisis risus, sed hendrerit odio.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col-3 -->
							
							<div class="col-3 col">							
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/bin/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<span class="h3-style title">Morbi malesuada nibh non blandit semper</span>
											<span class="h5-style light subtitle">Etiam enim lorem, aliquam a iaculis</span>
											<p>Donec nec sodales ante. Integer nisi mi, mattis ac accumsan sed, semper sed velit. Pellentesque at porttitor velit. Donec ac facilisis risus, sed hendrerit odio.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col-3 -->

						
							<div class="col-3 col">
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/bin/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<span class="h3-style title">Morbi malesuada nibh non blandit semper</span>
											<span class="h5-style light subtitle">Etiam enim lorem, aliquam a iaculis</span>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col-3 -->
							
							<div class="col-3 col">							
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/bin/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<span class="h3-style title">Morbi malesuada nibh non blandit semper</span>
											<span class="h5-style light subtitle">Etiam enim lorem, aliquam a iaculis</span>
											<p>Donec nec sodales ante. Integer nisi mi, mattis ac accumsan sed, semper sed velit. Pellentesque at porttitor velit. Donec ac facilisis risus, sed hendrerit odio.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col-3 -->
							
							<div class="col-3 col">							
								<div class="item">
								
									<a class="block with-img" href="#">
										<div class="img-wrap">
											<div class="img" style="background-image: url(../assets/bin/images/temp/featured-promotion-block-1.jpg);"></div>
										</div><!-- .img-wrap -->
										<div class="content">
										
											<div class="article-head">
												<time datetime="2015-10-23">September 23, 2015</time>
											</div>
										
											<span class="h3-style title">Morbi malesuada nibh non blandit semper</span>
											<span class="h5-style light subtitle">Etiam enim lorem, aliquam a iaculis</span>
											<p>Donec nec sodales ante. Integer nisi mi, mattis ac accumsan sed, semper sed velit. Pellentesque at porttitor velit. Donec ac facilisis risus, sed hendrerit odio.</p>
											<span class="button">Read More</span>
										</div><!-- .content -->
									</a><!-- .block -->
									
								</div><!-- .item -->
							</div><!-- .col-3 -->
							
						</div><!-- .grid.eqh -->

					</div><!-- .article-body -->
				
				</div><!-- .sw -->
			</div><!-- .filter-contents -->
		</div><!-- .filter-section -->
		
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>