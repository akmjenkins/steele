<?php $bodyclass = 'hotel-page'; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/bin/images/temp/hero/hero-inside-10.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
							
								<div class="hotel-logo">
									<img src="../assets/bin/images/hotels/glynmill-inn-light.svg" alt="The Glynmill Inn">
								</div><!-- .hotel-logo -->
								
								<div class="content">
									<h1>Map</h1>
									
									<p>
										Curabitur auctor aliquet ipsum et tincidunt. Maecenas convallis interdum ullamcorper. 
										Sed accumsan eros augue, id rutrum dolor convallis non. Mauris malesuada ultrices eleifend. 
										Curabitur id justo quis eros ornare porta a quis tellus.
									</p>
									
									<a href="#" class="button dark-bg">Book Now</a>
								</div><!-- .content -->
							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
		<div class="sw">
			<div class="hotel-body cf">
			
				<div class="hotel-nav">
				
					<div class="selector with-arrow">
						<select>
							<option value="">Select a Page</option>
							<option value="#url-to-page">Accommodations</option>
							<option value="#url-to-page">Ammenities</option>
							<option value="#url-to-page">Dining</option>
							<option value="#url-to-page">How to Get Here</option>
							<option value="#url-to-page">Banquets &amp; Catering</option>
							<option value="#url-to-page">Activities</option>
							<option value="#url-to-page" selected>Map</option>
						</select>
						<span class="value">&nbsp;</span>
					</div><!-- .selector -->
					
					<a href="#" class="sprite-before bed" data-page="accommodations" data-src="templates/inc/hotels/hotel-accommodations-partial.php">Accommodations</a>
					<a href="#" class="sprite-before wifi" data-page="ammenities" data-src="templates/inc/hotels/hotel-ammenities-partial.php">Ammenities</a>
					<a href="#" class="sprite-before cutlery" data-page="dining" data-src="templates/inc/hotels/hotel-dining-partial.php">Dining</a>
					<a href="#" class="sprite-before plane" data-page="how-to-get-here" data-src="templates/inc/hotels/hotel-directions-partial.php">How to Get Here</a>
					<a href="#" class="sprite-before platter" data-page="banquets" data-src="templates/inc/hotels/hotel-banquest-partial.php">Banquets &amp; Catering</a>
					<a href="#" class="sprite-before activities" data-page="activities" data-src="templates/inc/hotels/hotel-activities-partial.php">Activities</a>
					<a href="#" class="sprite-before pin selected" data-page="map" data-src="templates/inc/hotels/hotel-map-partial.php">Map</a>
					
					<div class="hotel-nav-extras-list">
						<span class="title sprite-after arrow-down toggle">Related Links</span>
						
						<ul>
							<li><a href="#">Related Link 1</a></li>
							<li><a href="#">Related Link 2</a></li>
							<li><a href="#">Related Link 3</a></li>
						</ul>
					</div><!-- .other-hotels -->
					
				</div><!-- .hotel-nav -->
				
				<div class="hotel-content article-body">
				
					<!-- loader -->
					<span class="loader sprite-before abs spinner"><img src="../assets/bin/images/spinner.gif" alt="spinner"></span>
				
					<div class="hotel-pages">
						<div data-page="map">
							<?php include('inc/hotels/hotel-map-partial.php'); ?>
						</div>
					</div><!-- .hotel-pages -->
				</div><!-- .hotel-content -->
			
			</div><!-- .hotel-body -->
		</div><!-- .sw -->
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>