<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/bin/images/temp/hero/hero-inside-9.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Photos &amp; Videos</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="sprite ib home replace">Home</a>
				<a href="#">Media</a>
				<a href="#">Photos &amp; Videos</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="filter-section">
		
			<div class="tab-wrapper">
		
				<div class="filter-bar dark-bg tab-controls">
					<div class="sw">
						<div class="meta">
						
							<div class="selector with-arrow">
								<select name="sort-by">
									<option>Sort By Hotel</option>
									<option value="jag">JAG</option>
									<option value="capital">The Capital Hotel</option>
									<option value="albatross">The Albatross</option>
									<option value="sinbads">Sinbad's</option>
									<option value="irving-west">The Irving West</option>
									<option value="glynmill-inn">The Glynmill Inn</option>
									<option value="hotel-gander">Hotel Gander</option>								
								</select>
								<span class="value">&nbsp;</span>
							</div><!-- .selector -->
						
							<form action="/" method="post" class="search-form single-form">
								<fieldset>
									<input type="text" name="s" placeholder="Search news...">
									<button type="submit" class="sprite-after abs search">Search</button>
								</fieldset>
							</form>
							
							<div class="controls">
								<button class="sprite arrow-prev-white">Previous</button>
								<button class="sprite arrow-next-white">Next</button>
							</div><!-- .controls -->
							
						</div><!-- .meta -->
					
						<div class="tab-control-wrap">
							<button class="tab-control selected">Photos</button>
							<button class="tab-control">Videos</button>
						</div>
					
					</div><!-- .sw -->
				</div><!-- .filter-bar -->
				
				<div class="tab-holder">
				
					<div class="tab selected">
				
						<div class="filter-contents">
							<div class="sw">
							
								<div class="grid">
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/1.jpg" data-tile="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/2.jpg"  data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/5.jpg"   data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/1.jpg" data-tile="Photo #5" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/1.jpg" data-tile="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/2.jpg"  data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/5.jpg"   data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/1.jpg" data-tile="Photo #5" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/1.jpg" data-tile="Photo #1" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/2.jpg"  data-title="Photo #2" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/7.jpg" data-title="Photo #3" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/5.jpg"   data-title="Photo #4" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-5 col sm-col-3 xs-col-2">
										<div>
											<a href="../assets/bin/images/temp/photos-videos/1.jpg" data-tile="Photo #5" data-gallery="photo-gallery" class="mpopup pad-thumb square" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
								</div><!-- .grid -->

							</div><!-- .sw -->
						</div><!-- .filter-contents -->
						
					</div><!-- .tab -->
					
					<div class="tab">
					
						<div class="filter-contents">
							<div class="sw">
							
								<div class="grid">
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #5" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #5" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #1" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #2" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/2.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #3" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/7.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-title="Video #4" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/5.jpg);"></a>
										</div>
									</div><!-- .col -->
									<div class="col-3 col sm-col-2 xs-col-1">
										<div>
											<a href="https://vimeo.com/105192180" data-title="Video #5" data-gallery="video-gallery" class="mpopup sprite-before abs video pad-thumb landscape" style="background-image: url(../assets/bin/images/temp/photos-videos/1.jpg);"></a>
										</div>
									</div><!-- .col -->
								</div><!-- .grid -->
							
							</div><!-- .sw -->
						</div><!-- .filter-contents -->
						
					</div><!-- .tab -->
				
				</div><!-- .tab-holder -->
			</div><!-- .tab-wrapper -->
			
	</div><!-- .filter-section -->
				
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>