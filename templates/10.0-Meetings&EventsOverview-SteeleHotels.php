<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

	<div class="hero single">
		<div class="swiper-wrapper">
			<div class="swipe" data-controls="true" data-auto="7">
				<div class="swipe-wrap">

					<div data-src="../assets/bin/images/temp/hero/hero-inside-9.jpg">
						<div class="item">&nbsp;</div>
						
						<div class="caption">
							<div class="sw">
								<h1 class="title">Photos &amp; Videos</h1>
								<span class="sub">Across the Island</span>

							</div><!-- .sw -->
						</div><!-- .caption -->
					</div>
					
				</div><!-- .swipe-wrap -->
			</div><!-- .swipe -->
		</div><!-- .swiper-wrapper -->
	</div><!-- .hero -->			

	<div class="body">
	
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="sprite ib home replace">Home</a>
				<a href="#">Media</a>
				<a href="#">Photos &amp; Videos</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="article-body">
		
			<section class="nopad split-block-container">
				<div class="sw">
					<div class="split-block img-left">
					
						<div class="img-wrap" style="background-image: url(../assets/bin/images/temp/hotel-meetings/1.jpg);"></div>
					
						<div class="content">
							
							<div class="hgroup">
								<h2>Meetings</h2>
								<h5>Lorem Ipsum Dolor Sit Amet</h5>
							</div><!-- .hgroup -->
							
							<p>
								Fusce quam ligula, mollis at ornare id, fermentum non nisi. Praesent vel dui rutrum urna fringilla vulputate sed ut dui. Sed nec lacinia elit, non lacinia nulla. 
							</p>
							
							<a href="#" class="button">Read More</a>
							
						</div><!-- .content -->
					</div><!-- .split-block -->
				</div><!-- .sw -->
			</section>
			
			<section class="nopad split-block-container">
				<div class="sw">
					<div class="split-block img-right">
					
						<div class="img-wrap" style="background-image: url(../assets/bin/images/temp/hotel-meetings/2.jpg);"></div>
					
						<div class="content">
							
							<div class="hgroup">
								<h2>Special Events</h2>
								<h5>Lorem Ipsum Dolor Sit Amet</h5>
							</div><!-- .hgroup -->
							
							<p>
								Fusce quam ligula, mollis at ornare id, fermentum non nisi. Praesent vel dui rutrum urna fringilla vulputate sed ut dui. Sed nec lacinia elit, non lacinia nulla. 
							</p>
							
							<a href="#" class="button">Read More</a>
							
						</div><!-- .content -->
					</div><!-- .split-block -->
				</div><!-- .sw -->
			</section>
			
			<section class="nopad split-block-container">
				<div class="sw">
					<div class="split-block img-left">
					
						<div class="img-wrap" style="background-image: url(../assets/bin/images/temp/hotel-meetings/3.jpg);"></div>
					
						<div class="content">
							
							<div class="hgroup">
								<h2>Weddings &amp; Banquets</h2>
								<h5>Lorem Ipsum Dolor Sit Amet</h5>
							</div><!-- .hgroup -->
							
							<p>
								Fusce quam ligula, mollis at ornare id, fermentum non nisi. Praesent vel dui rutrum urna fringilla vulputate sed ut dui. Sed nec lacinia elit, non lacinia nulla. 
							</p>
							
							<a href="#" class="button">Read More</a>
							
						</div><!-- .content -->
					</div><!-- .split-block -->
				</div><!-- .sw -->
			</section>
		
		</div><!-- .article-body -->
		
	</div><!-- .body -->
			

<?php include('inc/i-footer.php'); ?>