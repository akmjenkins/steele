//load all required scripts
requirejs(
	[
		'scripts/components/placeholder',
		'scripts/components/anchors.external.popup',
		'scripts/components/standard.accordion',
		'scripts/components/gmap',
		'scripts/components/custom.select',
		'scripts/swipe.srf',
		
		'scripts/components/magnific.popup',
		
		//puts .twox and .threex classes on the HTML tag for dealing with sprites
		'scripts/device.pixel.ratio',
		
		'scripts/search',
		'scripts/reservation.form',

		'scripts/nav',
		'scripts/hero'
	],
	function() {		
	
		//DEV REMOTE
		var GOOGLE_MAPS_API_KEY = 'AIzaSyA_Isk3ol7n97lO-VtdNOsc8hC0h6ldSQk';
	
		//IE8 doesn't support SVGs
		if(!Modernizr.svg) {
			$('img').each(function() {
				var src = this.src;
				if(src.indexOf('.svg') !== -1) {
					this.src = src.replace(/\.svg$/,'.png');
				}
			});
		}
		
		//contact page
		$('form.contact-form').each(function() {
			var 
				el  =$(this),
				selector = $('select[name=location]',el),
				locations = $('div.loc'),
				updateSelectedIndex = function(i) {
					selector[0].selectedIndex = i+1;
					locations.removeClass('selected').eq(i).addClass('selected');
				};
				
				selector.on('change',function(e) { updateSelectedIndex(this.selectedIndex); });
				locations.on('click',function(e) { e.target.nodeName === 'A' || updateSelectedIndex($(this).parent().index()); });
				
		});
		
		//event map
		$('#view-map')
			.on('click',function(e) {
				$('div.event-map').each(function() {
					var
						el = $(this),
						map = el.children('div.map').data('map')
						
					el.toggleClass('visible');
					google.maps.event.trigger(map.map,'resize');
					map.map.setCenter(map.mapOptions.center);
				});
			});
			
		//blocks
		$(window)
			.on('load resize',function() {
				var blocks = $('.blocks').filter(function() { return !$(this).hasClass('always-list'); });
				
				if(window.outerWidth < 650) {
					blocks.addClass('list-view');
					return;
				}
				
				blocks.each(function(i,blockGroup) {
					var collapseSize = blockGroup.className.match(/collapse-(\d+)/);
					if(collapseSize.length == 2 && window.outerWidth < (+collapseSize[1])) {
						$(blockGroup).addClass('list-view');
					} else {
						$(blockGroup).removeClass('list-view');
					}
				});
				
			}).trigger('resize');
		
		//extras list
		$('div.hotel-nav-extras-list').each(function() {
			var el = $(this);
			el.on('click','span.toggle',function(e) {
				el.toggleClass('expanded');
			});
		});
		
		//responsive hotel nav
		$('div.hotel-nav select').on('change',function(e) {
			if(this.value.length && this.value !== window.location.href) {
				window.location.href = this.value;
			}
		});
		
		//hotel loading pages from sidebar
		(function() {
		
			var methods = {
			
				hotelAnchors: null,
				hotelPagesContainer: null,
				
				getHotelPagesContainer: function() {
					if(!this.hotelPagesContainer) {
						this.hotelPagesContainer = $('div.hotel-pages');
					}
					
					return this.hotelPagesContainer;
				},
			
				getPageElements: function() {
					return this.getHotelPagesContainer().children();
				},
				
				getContent: function(url) {
					return $.ajax({url: templateJS.templateURL+url});
				},
				
				getAnchors: function() {
					if(!this.hotelAnchors) {
						this.hotelAnchors = $('div.hotel-nav a');
					}
					
					return this.hotelAnchors;
				},
				
				getAnchorForPage: function(page) {
					return this.getAnchors().filter(function() { return $(this).data('page') == page });
				},
				
				getPageDiv: function(page) {
					var pageDiv = this.getPageElements().filter(function() { return $(this).data('page') == page });
					if(!pageDiv.length) {
						pageDiv = $('<div class="not-ready" data-page="'+page+'"/>');
						this.getHotelPagesContainer().append(pageDiv);
					}
					
					return pageDiv;
				},
				
				showLoading: function(show) {
					this
						.getHotelPagesContainer()
						.parent()
						[show ? 'addClass' : 'removeClass']('loading');
						
					this
						.getPageElements()
						.addClass('other')
						.removeClass('selected');
				},
				
				finishShowingPageElement: function(el) {
				
					this.showLoading(false);
				
					el
						.removeClass('other not-ready')
						.addClass('selected')
						.siblings()
						.addClass('other')
						.removeClass('selected');
					
					window.location.hash = '#sect-'+el.data('page');
				},
				
				showPageElement: function(page) {
					var 
						self = this,
						pageDiv = this.getPageDiv(page),
						anchor = this.getAnchorForPage(page);
						
						this.getAnchors().removeClass('selected');
						anchor.addClass('selected')
						
					if(pageDiv.hasClass('not-ready')) {
					
						this.showLoading(true);
						
						this.getContent(anchor.data('src'))
							.done(function(r,status,jqXHR) {
								self.finishShowingPageElement(pageDiv.html(r));
							})
							.fail(function() {
								alert('Could not find '+url);
							});
					} else {
						self.finishShowingPageElement(pageDiv);
					}
				}
			
			};
			
			//this portion of code only to be active on the hotels page
			if(!methods.getAnchors().length) {
				return;
			}
			
			
			//small screens won't show this nav so we won't have to worry about them
			$(document)
				.on('click','div.hotel-nav a',function() {
					var page = $(this).data('page');
					if(page) {
						methods.showPageElement(page);
						return false;
					}
				});
			
			//get the hash of the page, if any exists
			if(window.location.hash) {
				methods.showPageElement(window.location.hash.replace('#sect-',''));
			//else, if we're on the hotels page
			} else {
				//always show the first one on page load
				methods.finishShowingPageElement(methods.getPageElements().eq(0));
			}
			
			$(window)
				.on('hashchange',function(e) {
					methods.showPageElement(window.location.hash.replace('#sect-',''));
				});
		
		}());
		
		//hotel directions
		(function() {
			
			var methods = {
			
				streetMap: null,
			
				getStreetMap: function() {
					if(!this.streetMap) {
						this.streetMap = $('#street-map');
					}
					
					return this.streetMap;
				},
			
				buildIFrame: function(origin) {
				
					var source = "https://www.google.com/maps/embed/v1/directions?mode=driving";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&destination='+this.getStreetMap().data('destination');				
					source+='&origin='+origin;
					this.getStreetMap().html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
				
				},
				
				geolocate: function() {
					var self = this;
					this.getStreetMap().addClass('loading');
					navigator.geolocation.getCurrentPosition(
						function(position) {
							self.onGeolocateDone(position);
						},
						function(positionError) {
							self.onGeolocateFailed(positionError);
						}
					);
				},
				
				onGeolocateDone: function(position) {
					this.buildIFrame(position.coords.latitude+','+position.coords.longitude);
				},
				
				onGelocateFailed: function(positionError) {
					alert(positionError.message);
					this.getStreetMap().removeClass('loading');								
				}
			
			};
			
			$(document)
				.on('submit','#street-form',function() {
					methods.buildIFrame($(this).find('input').val());
					return false;
				})
				.on('click','#directions-geolocate',function() {
					methods.geolocate();
				});
			
		}());
		
		//hotel activities map
		(function() {
		
			var methods = {
			
				activitiesMap: null,
			
				buildIFrame: function(q) {
					var source = "https://www.google.com/maps/embed/v1/search?";
					source+='&key='+GOOGLE_MAPS_API_KEY;
					source+='&center='+this.getActivitiesMap().data('center');
					source+='&zoom='+this.getActivitiesMap().data('zoom');
					source+='&q='+q;
					this.getActivitiesMap().html('<iframe src="'+source+'" frameborder="0" style="border:0"/>');
				},
			
				getActivitiesMap: function() {
					if(!this.activitiesMap) {
						this.activitiesMap = $('#activities-map');
					}
					
					return this.activitiesMap;
				},
				
			};
			
			$(document)
				.on('submit','#activities-form',function() {
					methods.buildIFrame($(this).find('input').val());
					return false;
				});
			
		
		}());
		
});