define([
	'scripts/swipe.srf',
],function(SwipeSRF) {

	var 
		$document = $(document),
		$body = $('body'),
		$nav = $('nav'),
		SHOW_CLASS = 'show-nav',
		ANIMATING_CLASS = 'animating animating-nav';


	//mobile nav
	(function() {
	
		var cb = function(e) { 
			var el = e.target ? e.target : e.srcElement;
			
			if($(el).hasClass('page-wrapper')) {
				$body.removeClass(ANIMATING_CLASS);
			}
		}
		$document.on('click','#mobile-nav',function(e) {
			methods.toggleNav();
			return false;
		});
		
		$body
			.on({
				'webkitTransitionEnd': cb,
				'msTransitionEnd': cb,
				'oTransitionEnd': cb,
				'otransitionend': cb,
				'transitionend': cb
			});
		
	}());

	var methods = {

		showNav: function(show) {
			$body[show ? 'addClass' : 'removeClass'](SHOW_CLASS);

			if(Modernizr.csstransforms3d) {
				$body.addClass(ANIMATING_CLASS);
			}

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $body.hasClass(SHOW_CLASS);
		}

	};

	
	//sliders in nav dropdowns
	$nav
		.on('mouseenter','>ul>li',function(e) {
			var
				el  =$(this),
				dd = el.children('div'),
				swiperEl = dd.find('div.swipe'),
				swiper = swiperEl.data('swipe');
			
				//rebuild
				if(swiper) {
					swiper.setup();
					swiperEl.trigger('swipeChanged',[0,swiperEl.find('div.img').eq(0).parent()[0]]);
				}
				
		})
		.on('mouseenter','li li a',function(e) {
			var
				el = $(this),
				index = el.parent().index(),
				dd = el.closest('div'),
				swiper = dd.find('div.swipe');
				
				swiper.data('swipe').slide(index);
		})
		.on('swipeChanged',function(e,index,element) {
			var
				source,
				el = $(element),
				imgEl = el.find('div.img'),
				imgSrc = imgEl.data('img');
				
				if(imgEl.data('working')) { return; }
				
				imgEl.data('working',true);
				
				if(imgSrc) {
					source = templateJS.templateURL+'/'+imgSrc;
					$('<img/>')
						.on('load',function() {
							imgEl
								.css('backgroundImage','url('+source+')')
								.addClass('loaded')
								.removeData('working')
								.removeData('img');
						})
						.on('error',function() {
							imgEl.removeData('working');
						})
						.attr('src',source);
				}
				
		});
		

	//no public API
	return {};

});